use crate::point::Point;
use crate::render::Gradient;
use crate::EPSILON;

#[derive(Debug)]
pub struct Grid {
	pub side_length: u16,
	pub heights: Vec<f64>,
	min: f64,
	max: f64,
}

fn interpolate(x: u16, y: u16, res: u16, power: f64, samples: &[Point]) -> f64 {
	let p = [f64::from(x) / f64::from(res - 1),
			f64::from(y) / f64::from(res - 1)
	];
	let weight = |sx: f64, sy: f64| -> f64 {
		1.0 / ((p[0] - sx).powi(2) + (p[1] - sy).powi(2)).sqrt().powf(power)
	};

	// skip calculation if point is already in samples
	for s in samples {
		if (s.x - p[0]).abs() < EPSILON &&
			(s.y - p[1]).abs() < EPSILON
		{
			return s.z;
		}
	}

	let mut numerator = 0.0;
	let mut denominator = 0.0;
	for s in samples {
		let w = weight(s.x, s.y);
		numerator += w * s.z;
		denominator += w;
	}

	numerator / denominator
}

impl Grid {
	pub fn new(resolution: u16) -> Self {
		Self {
			side_length: resolution,
			heights: vec![0.0; (resolution * resolution).into()],
			max: 0.0,
			min: 0.0,
		}
	}

	pub fn from_samples(samples: &[Point], power: f64, resolution: u16) -> Self {
		let mut surface = Self {
			side_length: resolution,
			heights: Vec::with_capacity((resolution * resolution).into()),
			min: f64::NAN,
			max: f64::NAN,
		};

		for y in 0..resolution {
			for x in 0..resolution {
				let height = interpolate(x, y, resolution, power, samples);
				surface.min = surface.min.min(height);
				surface.max = surface.max.max(height);
				surface.heights.push(height);
			}
		}

		surface
	}

	pub fn color_at_index(&self, index: usize, gradient: &Gradient,
		min: f64, max: f64) -> [u8; 4]
	{
		if index >= self.heights.len() {
			panic!("Grid::pixel_at(): index out of range");
		}

		gradient.find_color(
			((self.heights[index] - min) / (max - min))
			.max(0.0).min(1.0))
	}

	pub fn get_min(&self) -> f64 {
		self.min
	}

	pub fn get_max(&self) -> f64 {
		self.max
	}
}



#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_flat() {
		let samples = vec![
			Point {x: 0.0, y: 0.0, z: 0.25},
		];
		let surface = Grid::from_samples(&samples, 2.0, 10);

		assert_eq!(surface.side_length, 10);
		assert_eq!(surface.heights.len(), 100);
		assert!((surface.max - 0.25).abs() < EPSILON);
		assert!((surface.min - 0.25).abs() < EPSILON);
		for point in surface.heights {
			assert!((point - 0.25).abs() < EPSILON);
		}
	}

	#[test]
	fn test_slope() {
		let samples = vec![
			Point {x: 0.0, y: 0.5, z: 0.25},
			Point {x: 1.0, y: 0.5, z: 0.75},
		];
		let surface = Grid::from_samples(&samples, 2.0, 3);
		let solution = Grid {
			side_length: 3,
			heights: vec![
				1.0 / 3.0,	0.5, 2.0 / 3.0,
				0.25,		0.5, 0.75,
				1.0 / 3.0,	0.5, 2.0 / 3.0,
			],
			max: 0.75,
			min: 0.25,
		};

		assert_eq!(solution.side_length, surface.side_length);
		assert_eq!(solution.heights.len(), surface.heights.len());
		assert!((solution.max - surface.max).abs() < EPSILON);
		assert!((solution.min - surface.min).abs() < EPSILON);
		for index in 0..9 {
			assert!((solution.heights.get(index).unwrap() -
				surface.heights.get(index).unwrap()).abs() < EPSILON);
		}
	}

	#[test]
	fn test_hill() {
		let samples = vec![
			Point {x: 0.0, y: 0.0, z: 0.0},
			Point {x: 1.0, y: 0.0, z: 0.0},
			Point {x: 0.0, y: 1.0, z: 0.0},
			Point {x: 1.0, y: 1.0, z: 0.0},
			Point {x: 0.5, y: 0.5, z: 0.4},
		];
		let surface = Grid::from_samples(&samples, 2.0, 15);

		assert_eq!(surface.side_length, 15);
		assert_eq!(surface.heights.len(), 225);
		assert!((surface.max - 0.0).abs() < EPSILON);
		assert!((surface.min - 0.4).abs() < EPSILON);
		for index in 0..15 * 15 {
			let point = surface.heights.get(index).unwrap();

			// corner points
			if index == 0 || index == 14 || index == 210 || index == 224 {
				assert_eq!(*point, 0.0);
			}

			// center point
			else if index == 112 {
				assert_eq!(*point, 0.4);
			}

			// all other points
			else {
				assert!(*point > 0.0 && *point < 0.4);
			}
		}
	}
}