use crate::surface::Grid;

#[derive(Clone)]
pub struct Gradient {
	colors: Vec<[u8; 4]>,
}

impl Gradient {
	pub fn new(colors: &Vec<[u8; 4]>) -> Self {
		Self {
			colors: colors.clone(),
		}
	}

	pub fn find_color(&self, position: f64) -> [u8; 4] {
		if position < 0.0 || position > 1.0 {
			panic!("find_color(): position out of range");
		}

		let len = self.colors.len();
		if len == 1 {
			return self.colors[0];
		}

		let section = position / (1.0 / (len - 1) as f64);
		let fraction = section - section.floor();
		let section = (section - fraction).round() as usize;

		if section == len - 1 {
			self.colors[section]
		} else {
			mix_colors(&self.colors[section],
				&self.colors[section + 1], fraction)
		}
	}

}

pub fn mix_colors(c1: &[u8; 4], c2: &[u8; 4], position: f64) -> [u8; 4] {
	[
		(c1[0] as f64 * (1. - position) + c2[0] as f64 * position).round() as u8,
		(c1[1] as f64 * (1. - position) + c2[1] as f64 * position).round() as u8,
		(c1[2] as f64 * (1. - position) + c2[2] as f64 * position).round() as u8,
		(c1[3] as f64 * (1. - position) + c2[3] as f64 * position).round() as u8,
	]
}

pub fn blend_colors(bg: &[u8], fg: &[u8]) -> [u8; 4] {
	let alpha_fg = fg[3] as f64 / 255.0;
	let alpha_bg = (bg[3] as f64 / 255.0) * (1.0 - alpha_fg);
	let alpha_out = alpha_fg + alpha_bg;

	[
		(((fg[0] as f64) * alpha_fg + (bg[0] as f64) * alpha_bg) / alpha_out) as u8,
		(((fg[1] as f64) * alpha_fg + (bg[1] as f64) * alpha_bg) / alpha_out) as u8,
		(((fg[2] as f64) * alpha_fg + (bg[2] as f64) * alpha_bg) / alpha_out) as u8,
		(alpha_out * 255.0) as u8,
	]
}

pub fn render_dry_surface(surface: &Grid, gradient: &Gradient) -> Vec<u8>
{
	let mut pixels = Vec::with_capacity(surface.heights.len() * 4);
	for index in 0..surface.heights.len() {
		let color = surface.color_at_index(index, gradient,
			surface.get_min(), surface.get_max());
		pixels.push(color[0]);
		pixels.push(color[1]);
		pixels.push(color[2]);
		pixels.push(color[3]);
	}

	pixels
}