use crate::surface::Grid;
use crate::render::{self, Gradient};
use std::str::FromStr;
use gif::Frame;
use rand::Rng;

// minimum water height for a point to be able to spread water
const MINIMUM_WATER_LEVEL: f64 = 1e-3;

// fraction of a points volume that is given to adjacent points each tick
const SPREAD_ADJACENT: f64 = 0.18;
const SPREAD_DIAGONAL: f64 = 0.02;

// height that the rising flood gains per tick
const DEFAULT_SPEED: f64 = 1e-2;

// how high the left column gets refilled each tick in a wave
const WAVE_HEIGHT: f64 = 0.8;

// amount of rain particles per 10*10 square
const RAIN_DENSITY: u32 = 10;

// amount of water that a rain drop adds
const RAIN_VOLUME: f64 = 0.1;

// any amount below this will get rendered like no water
const WATER_MIN: f64 = 0.01;

// any amount above this will get rendered like maximum water
const WATER_MAX: f64 = 0.2;

#[derive(Debug, Clone, Copy)]
pub enum SimulationKind {
	Rise,
	Wave,
	Rain,
}

pub struct Simulation {
	land: Grid,
	land_rendered: Vec<u8>,
	water: Grid,
	water_colors: Gradient,
	water_level: f64,
	kind: SimulationKind,
	duration: u32,
	ticks_per_frame: u32,
	quality: i32,
	speed: f64,
}

impl FromStr for SimulationKind {
	type Err = String;

	fn from_str(input: &str) -> Result<Self, Self::Err> {
		match input {
			"Rise" => Ok(Self::Rise),
			"rise" => Ok(Self::Rise),
			"Wave" => Ok(Self::Wave),
			"wave" => Ok(Self::Wave),
			"Rain" => Ok(Self::Rain),
			"rain" => Ok(Self::Rain),
			_ => Err(format!("unrecognized simulation kind: {}", input)),
		}
	}
}

impl Simulation {
	pub fn new(
		land: Grid,
		kind: SimulationKind,
		land_colors: &Gradient,
		water_colors: Gradient,
		duration: u32,
		ticks_per_frame: u32,
		quality: i32,
		speed: f64) -> Self
	{
		if quality < 1 || quality > 30 {
			panic!("Simulation::new(): quality out of range");
		}

		Self {
			land_rendered: render::render_dry_surface(&land, land_colors),
			water: Grid::new(land.side_length),
			speed: DEFAULT_SPEED * speed,
			water_level: 0.0,
			ticks_per_frame,
			water_colors,
			duration,
			quality,
			land,
			kind,
		}
	}

	pub fn render(&self) -> Vec<u8> {
		let mut pixels = Vec::with_capacity(self.land_rendered.len());

		for index in 0..self.land.heights.len() {
			let color = render::blend_colors(
				&self.land_rendered[4 * index..4 * index + 4],
				&self.water.color_at_index(index, &self.water_colors,
					WATER_MIN, WATER_MAX));
			pixels.push(color[0]);
			pixels.push(color[1]);
			pixels.push(color[2]);
			pixels.push(color[3]);
		}

		pixels
	}

	pub fn next_tick(&mut self) -> gif::Frame<'static> {
		self.duration -= 1;
		match &self.kind {
			SimulationKind::Rise => {self.tick_rise()},
			SimulationKind::Wave => {self.tick_wave(); self.tick_physics()},
			SimulationKind::Rain => {self.tick_rain(); self.tick_physics()},
		}

		Frame::from_rgba_speed(self.land.side_length, self.land.side_length,
			&mut self.render(), self.quality)
	}

	fn tick_rise(&mut self) {
		self.water_level += self.speed;

		for index in 0..self.land.heights.len() {
			if self.land.heights[index] + self.water.heights[index] <
				self.water_level
			{
				self.water.heights[index] =
					self.water_level - self.land.heights[index];
			}
		}
	}

	fn tick_wave(&mut self) {
		for y in 0..self.water.side_length {
			let point = &mut self.water.heights[(y * self.water.side_length) as usize];
			//*point = (*point).max(WAVE_HEIGHT);
			*point += WAVE_HEIGHT;
		}
	}

	fn tick_rain(&mut self) {
		let mut rng = rand::thread_rng();
		let size = self.water.side_length as usize;

		for _i in 0..RAIN_DENSITY {
			let x = rng.gen_range(0..size);
			let y = rng.gen_range(0..size);

			self.water.heights[y * size + x] += RAIN_VOLUME;
		}
	}

	/// Each point, if it holds more water than `MINUMUM_WATER_LEVEL`, will
	/// try to spread up to 10% of its volume with each directly adjacent
	/// and up to 1% with each diagonally adjacent point.
	/// These differences get written to a buffer first, and applied to all points
	/// after calculations are complete.
	fn tick_physics(&mut self) {
		let mut temp_buffer = vec![0.0; self.water.heights.len()];
		let size = self.water.side_length as usize;

		for y in 0..size {
			for x in 0..size {
				if self.water.heights[y * size + x] < MINIMUM_WATER_LEVEL {
					continue;
				}

				let adjacent = self.water.heights[y * size + x] * SPREAD_ADJACENT;
				let diagonal = self.water.heights[y * size + x] * SPREAD_DIAGONAL;
				if x > 0 {
					if y > 0 {
						self.share_water(&mut temp_buffer, x, y, x - 1, y - 1, diagonal);}
					self.share_water(&mut temp_buffer,     x, y, x - 1, y    , adjacent);
					if y + 1 < size {
						self.share_water(&mut temp_buffer, x, y, x - 1, y + 1, diagonal);}
				}

				if y > 0 {
					self.share_water(&mut temp_buffer,     x, y, x    , y - 1, adjacent);}
				if y + 1 < size {
					self.share_water(&mut temp_buffer,     x, y, x    , y + 1, adjacent);}

				if x + 1 < size {
					if y > 0 {
						self.share_water(&mut temp_buffer, x, y, x + 1, y - 1, diagonal);}
					self.share_water(&mut temp_buffer,     x, y, x + 1, y    , adjacent);
					if y + 1 < size {
						self.share_water(&mut temp_buffer, x, y, x + 1, y + 1, diagonal);}
				}
			}
		}

		for index in 0..temp_buffer.len() {
			self.water.heights[index] += temp_buffer[index];
		}
	}

	fn share_water(
		&mut self,
		buffer: &mut Vec<f64>,
		x1: usize,
		y1: usize,
		x2: usize,
		y2: usize,
		amount: f64)
	{
		let size = self.water.side_length as usize;
		let from = y1 * size + x1;
		let to = y2 * size + x2;
		let slope = self.water.heights[to] - self.water.heights[from];
		let actual = (amount - 2.0 * slope.max(0.0)).max(0.0);

		buffer[from] -= actual;
		buffer[to] += actual;
	}
}

impl Iterator for Simulation {
	type Item = Frame<'static>;

	fn next(&mut self) -> Option<Self::Item> {
		let mut result = None;

		for _i in 0..self.ticks_per_frame {
			result = match self.duration {
				0 => None,
				_ => Some(self.next_tick()),
			}
		}

		result
	}
}

