mod options;
mod point;
mod surface;
mod fluid;
mod render;

use crate::options::Opt;
use crate::point::Point;
use crate::surface::Grid;
use crate::fluid::*;
use crate::render::*;

use structopt::StructOpt;
use gif::Encoder;
use std::time::Instant;
use std::error::Error;
use std::fs::{self, File};
use std::path::PathBuf;

pub const EPSILON: f64 = 1e-10;

fn main() -> Result<(), Box<dyn Error>> {

    let opt = Opt::from_args();
    let land_colors = Gradient::new(&vec![
        [0,0,0,255], [100,100,200,255], [100,200,100,255], [255,255,255,255]]);
    let water_colors = Gradient::new(&vec![
        [0,0,255,0], [0,0,255,128], [0,0,255,128], [0,0,255,255], [0,0,255,255]]);

    for file in &opt.files {
        let started = Instant::now();
        if let Err(e) = options::check_file(file) {
            return Err(Box::new(e));
        }

        let buffer = fs::read_to_string(file)?;
        let samples = Point::from_str(&buffer)?;

        let mut outfile = PathBuf::new();
        match opt.dir {
            Some(ref dir) => {
                outfile.push(dir);
                outfile.push(file.file_name().unwrap())
            },
            None => {
                outfile.push(file)
            }
        }
        outfile.set_extension("gif");
        let outfile = File::create(outfile)?;

        let mut encoder = Encoder::new(
            outfile, opt.resolution, opt.resolution, &[])?;
        let sim = Simulation::new(
            Grid::from_samples(&samples, opt.smoothness, opt.resolution),
            opt.kind,
            &land_colors,
            water_colors.clone(),
            opt.frames * opt.ticks,
            opt.ticks,
            opt.quality,
            opt.speed
        );

        let mut counter = 0;
        for frame in sim {
            counter += 1;
            if opt.verbosity >= 2 {
                println!("Frame {} of {}...", counter, opt.frames);
            }

            encoder.write_frame(&frame)?;
        }

        if opt.verbosity >= 1 {
            println!("Rendered {} frames in {:.3} seconds.",
                opt.frames, started.elapsed().as_secs_f64());
        }
    }

    Ok(())
}
