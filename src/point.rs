use std::collections::VecDeque;
use std::io::{self, ErrorKind};
use std::error::Error;

#[derive(Debug, Clone)]
pub struct Point {
	pub x: f64,
	pub y: f64,
	pub z: f64,
}

fn normalize(points: &mut Vec<Point>) {
	let mut range_x = [f64::NAN, f64::NAN];
	let mut range_y = [f64::NAN, f64::NAN];
	let mut range_z = [f64::NAN, f64::NAN];
	let amount = points.len();
	let update_range = |range: &mut [f64; 2], value| {
		if range[0].is_nan() || value < range[0] {
			range[0] = value;
		}
		if range[1].is_nan() || value > range[1] {
			range[1] = value;
		}
	};

	for pt in points.clone() {
		update_range(&mut range_x, pt.x);
		update_range(&mut range_y, pt.y);
		update_range(&mut range_z, pt.z);
	}

	let scaling = (range_x[1] - range_x[0]).max(range_y[1] - range_y[0]);
	let update_point = |pt: &mut Point| {
		pt.x = ((pt.x - range_x[0]) / scaling).max(0.0).min(1.0);
		pt.y = ((pt.y - range_y[0]) / scaling).max(0.0).min(1.0);
		pt.z = ((pt.z - range_z[0]) / scaling).max(0.0).min(1.0);

		// only one point exists
		if amount == 1 {
			pt.x = 0.5;
			pt.y = 0.5;
			pt.z = 0.0;
		}

		// all points have same x
		else if range_x[0] == range_x[1] {
			pt.x = 0.5;
		}

		// all points have same y
		else if range_y[0] == range_y[1] {
			pt.y = 0.5;
		}
	};

	for pt in points {
		update_point(pt);
	}
}

impl Point {
	pub fn from_str(buffer: &str) -> Result<Vec<Point>, Box<dyn Error>> {
		let mut values = VecDeque::with_capacity(60);
		for word in buffer.split_whitespace() {
			values.push_back(word.parse::<f64>()?);
		}

		if values.len() % 3 != 0 || values.is_empty() {
			return Err(Box::new(
				io::Error::new(ErrorKind::InvalidInput, "Not enough values")
			));
		}

		let mut points = Vec::with_capacity(values.len() / 3);
		while !values.is_empty() {
			points.push(Point{
				x: values.pop_front().unwrap(),
				y: values.pop_front().unwrap(),
				z: values.pop_front().unwrap(),
			});
		}
		normalize(&mut points);

		Ok(points)
	}
}



#[cfg(test)]
mod tests {
	use super::*;
	use crate::EPSILON;

	fn points_eq(expected: &Point, actual: &Point) -> bool {
		(expected.x - actual.x).abs() < EPSILON &&
		(expected.y - actual.y).abs() < EPSILON &&
		(expected.z - actual.z).abs() < EPSILON
	}

	#[test]
	fn test_one_point() -> Result<(), Box<dyn Error>> {
		let points = Point::from_str("1.0 2 3.")?;

		assert_eq!(points.len(), 1);
		assert_eq!(points[0].x, 0.5);
		assert_eq!(points[0].y, 0.5);
		assert_eq!(points[0].z, 0.0);

		Ok(())
	}

	#[test]
	fn test_valley() -> Result<(), Box<dyn Error>> {
		let points = Point::from_str(
			"-3 .5 0   -2.5 .5 0   -3 1 0   -2.5 1 0   -2.75 .75 -.15")?;
		let solution = vec![
			Point{x: 0.0, y: 0.0, z: 0.3},
			Point{x: 1.0, y: 0.0, z: 0.3},
			Point{x: 0.0, y: 1.0, z: 0.3},
			Point{x: 1.0, y: 1.0, z: 0.3},
			Point{x: 0.5, y: 0.5, z: 0.0},
		];

		assert_eq!(points.len(), solution.len());
		for index in 0..5 {
			assert!(points_eq(&solution[index], &points[index]));
		}

		Ok(())
	}

	#[test]
	fn test_points_with_equal_x() -> Result<(), Box<dyn Error>> {
		let points = Point::from_str("20 .1 0   20 .2 -.2   20 .6 0")?;
		let solution = vec![
			Point{x: 0.5, y: 0.0, z: 0.4},
			Point{x: 0.5, y: 0.2, z: 0.0},
			Point{x: 0.5, y: 1.0, z: 0.4},
		];

		assert_eq!(points.len(), 3);
		for index in 0..3 {
			assert!(points_eq(&solution[index], &points[index]));
		}

		Ok(())
	}

	#[test]
	fn test_points_with_equal_y() -> Result<(), Box<dyn Error>> {
		let points = Point::from_str(".1 20 0   .2 20 -.2   .6 20 0")?;
		let solution = vec![
			Point{x: 0.0, y: 0.5, z: 0.4},
			Point{x: 0.2, y: 0.5, z: 0.0},
			Point{x: 1.0, y: 0.5, z: 0.4},
		];

		assert_eq!(points.len(), 3);
		for index in 0..3 {
			assert!(points_eq(&solution[index], &points[index]));
		}

		Ok(())
	}
}