use std::io::{self, ErrorKind};
use std::path::PathBuf;
use structopt::StructOpt;
use crate::fluid::SimulationKind;

#[derive(StructOpt, Debug)]
#[structopt(name = "mod1", about = "A simple \x1B[9mwater\x1B[0m blue goop simulator.")]
pub struct Opt {
    /// Resolution of the simulation and the output gif
    #[structopt(short, long, default_value="10")]
    pub resolution: u16,

    /// Smoothness of the surface, smaller values give a smoother result
    #[structopt(short, long, default_value="2.0")]
    pub smoothness: f64,

    /// Number of frames in the gif
    #[structopt(short, long, default_value="30")]
    pub frames: u32,

    /// How many ticks of the simulation are done per frame
    /// A value of 1 should be fine for most cases, but for waves, something
    /// like 10 is recommended, though that drastically increases render time.
    #[structopt(short, long, default_value="1")]
    pub ticks: u32,

    /// Quality of the output gif in [1, 30]
    #[structopt(short, long, default_value="10")]
    pub quality: i32,

    /// How fast the water raises for the Rise simulation
    #[structopt(long = "rise-speed", default_value="1")]
    pub speed: f64,

    /// The type of simulation to render
    #[structopt(short, long, default_value="Rise")]
    pub kind: SimulationKind,

    /// The directory to save the gifs in. Defaults to the directory the corresponding
    /// input file is in.
    #[structopt(short, long)]
    pub dir: Option<PathBuf>,

    /// -v: show render times for files
    /// -vv: print a message for each frame in progress
    #[structopt(short="v", parse(from_occurrences))]
    pub verbosity: u8,

    /// Input files, the extension needs to be `.mod1`
    #[structopt(name = "FILE", parse(from_os_str))]
    pub files: Vec<PathBuf>,
}

pub fn check_file(filepath: &PathBuf) -> Result<(), io::Error> {
    let name = match filepath.file_name() {
        None => {return Err(io::Error::new(ErrorKind::Other,
            "argument is directory"));}
        Some(n) => n
    };

    match name.to_str().unwrap().ends_with(".mod1") {
        true => Ok(()),
        false => Err(io::Error::new(ErrorKind::InvalidInput,
            "unrecognized file extension"))
    }
}